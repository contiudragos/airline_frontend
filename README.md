# Airlane Company Frontend
### The main purpose of the project

The main purpose for this project was to learn basic Kotlin and Android commands, controls, structures, feeling more comfortable working with java
and Requests and Responses considering the backend that was in another technology. Also, Postman was a grate tool to use, and it's for sure a good practice for the future. Unit tests were a
big part of the project because they gave me the worst nightmares. :)

### Endpoints that you can access from frontend
- http://localhost:8080/api/v1/airplane
- http://localhost:8080/api/v1/airport
- http://localhost:8080/api/v1/employee
- http://localhost:8080/api/v1/flight

All endpoints have 4 methods implemented: GET, POST, PUT, DELETE

- GET - gives you all the entities from the database
- POST - inserts an entity if all the details given are correct and a status of 200 or 400 if something went wrong
- PUT - updates an entity if all the details given are correct and a status of 200 or 400 if something went wrong
- DELETE - deletes an entity if all the details given are correct and a status of 200 or 400 if something went wrong

![flow_diagram](Front-flow.png)

The User has all the right to alter the data. He is entitled to add/delete/see(get)/update each airplane, airport, employee and flight.

### Good thing to know
For all insert, update and delete methods that are implemented, there are validations in order to keep the data
as clean as possible. If you get an 400 with a custom message, ypu probably entered some malicious data.

Whenever the User enter wrong data, a message will tell him this. Also, each successful operation is shown in a separate success message.


### Used components
Most used components are TextView, EditView, DropDowns and Buttons. For the connection with the API, I used Retrofit with GSON converter. 


### Improvements for the future
* Finish all the endpoints
    * Employee
    * Flight
* Make the interface more user-friendly
    * Add better design
    * Transform the data that is shown in dropdowns and lists
* Improve error messages
* Add better validation
* Use fragments instead of activities
* Write reusable code
* Wire code that respects principles and clean code

