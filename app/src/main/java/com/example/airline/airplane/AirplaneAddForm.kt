package com.example.airline.airplane

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.airline.R
import retrofit2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDate

class AirplaneAddForm : AppCompatActivity() {

    /**
     * This function renders the form which allows the user to add data
     * Check the validity of the data
     * Send the request to the backend
     * Handle the response received
     */
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airplane_add_form)

        val submitButton = findViewById<Button>(R.id.btn_add_airplane)
        val model = findViewById<EditText>(R.id.airplane_model)
        val date = findViewById<DatePicker>(R.id.airplane_date)
        val capacity = findViewById<EditText>(R.id.airplane_capacity)
        val uniqueId = findViewById<EditText>(R.id.airplane_unique_id)


        submitButton.setOnClickListener {
            val today = LocalDate.now()
            val givenDate = LocalDate.of(date.year, date.month, date.dayOfMonth)

            if (today < givenDate) {
                Toast.makeText(this@AirplaneAddForm, "The date is in the future", Toast.LENGTH_LONG)
                    .show();
            } else {
                val airplane = AirplaneDataItem(
                    -1,
                    model?.text.toString(),
                    "" + date.year + "-" + date.month + "-" + date.dayOfMonth,
                    Integer.parseInt(capacity.text.toString()),
                    uniqueId.text.toString()
                )

                val retrofitBuilder = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://10.0.2.2:8080/api/v1/")
                    .build()
                    .create(AirplaneApiInterface::class.java)

                val response = retrofitBuilder.postData(airplane)
                response.enqueue(object : Callback<AirplaneDataItem> {
                    override fun onResponse(
                        call: Call<AirplaneDataItem>,
                        response: Response<AirplaneDataItem>
                    ) {
                        if (response.isSuccessful) {
                            Toast.makeText(
                                this@AirplaneAddForm,
                                "Successfully added",
                                Toast.LENGTH_LONG
                            ).show();
                        } else {
                            Toast.makeText(
                                this@AirplaneAddForm,
                                "There is already a plane with this id",
                                Toast.LENGTH_LONG
                            ).show();
                        }
                    }

                    override fun onFailure(call: Call<AirplaneDataItem>, t: Throwable) {
                        Log.d("Error", t.message.toString())
                    }

                })
            }
        }
    }
}
