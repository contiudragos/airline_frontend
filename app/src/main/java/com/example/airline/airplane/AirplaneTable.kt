package com.example.airline.airplane

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AirplaneTable : AppCompatActivity() {

    /**
     * Call the method that gets all the airplanes
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airplane_table)

        getAirplanes()
    }

    /**
     * Gets all the airplanes and mount them into a list
     */
    private fun getAirplanes() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirplaneApiInterface::class.java)

        val data = retrofitBuilder.getData()

        data.enqueue(object : Callback<List<AirplaneDataItem>?> {
            override fun onResponse(
                call: Call<List<AirplaneDataItem>?>,
                response: Response<List<AirplaneDataItem>?>
            ) {
                val responseBody = response.body()!!

                val table = findViewById<TableLayout>(R.id.table_airplane)

                for (data in responseBody) {
                    val tableRow = TableRow(this@AirplaneTable)
                    tableRow.setPadding(0, 5, 0, 5)
                    val id = TextView(this@AirplaneTable)
                    id.text = data.id.toString()
                    id.setTextColor(Color.WHITE)
                    id.textSize = 12f

                    tableRow.addView(id)

                    val model = TextView(this@AirplaneTable)
                    model.text = data.model
                    model.textSize = 12f
                    model.setTextColor(Color.WHITE)
                    tableRow.addView(model)

                    val capacity = TextView(this@AirplaneTable)
                    capacity.text = data.capacity.toString()
                    capacity.setTextColor(Color.WHITE)
                    capacity.textSize = 12F
                    tableRow.addView(capacity)

                    val productionDate = TextView(this@AirplaneTable)
                    productionDate.text = data.productionDate
                    productionDate.setTextColor(Color.WHITE)
                    productionDate.textSize = 12f
                    tableRow.addView(productionDate)

                    val uniqueId = TextView(this@AirplaneTable)
                    uniqueId.text = data.uniqueId
                    uniqueId.setTextColor(Color.WHITE)
                    uniqueId.textSize = 12f
                    tableRow.addView(uniqueId)

                    table.addView(tableRow)
                }

            }

            override fun onFailure(call: Call<List<AirplaneDataItem>?>, t: Throwable) {
                Log.d("MainActivity", "Error while fetching " + t.message)
            }
        })
    }
}