package com.example.airline.airplane

import android.R
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.Long
import kotlin.Throwable


class AirplaneDelete : AppCompatActivity() {
    var idSelectedProduct: Long = 0

    /**
     * Creates a view that displays all the airplanes
     * Select an airplane an display a message
     * Call the delete method from the interface
     * Handle the response
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.airline.R.layout.activity_airplane_delete)

        val delete = findViewById<View>(com.example.airline.R.id.deleleDeleteProductBtn)
        val listView: ListView = findViewById(com.example.airline.R.id.listViewDelete)
        var list: List<AirplaneDataItem> = ArrayList()

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirplaneApiInterface::class.java)

        val data = retrofitBuilder.getData()

        data.enqueue(object : Callback<List<AirplaneDataItem>?> {
            override fun onResponse(
                call: Call<List<AirplaneDataItem>?>,
                response: Response<List<AirplaneDataItem>?>
            ) {
                val products: List<AirplaneDataItem>? = response.body()
                if (products != null) {
                    list = products
                }
                val arrayAdapter: ArrayAdapter<AirplaneDataItem?> = ArrayAdapter<AirplaneDataItem?>(
                    applicationContext, R.layout.simple_list_item_1, list
                )
                listView.adapter = arrayAdapter
            }

            override fun onFailure(call: Call<List<AirplaneDataItem>?>, t: Throwable) {
                Toast.makeText(this@AirplaneDelete, "No airplanes in db", Toast.LENGTH_LONG).show();
            }
        })

        listView.onItemClickListener =
            OnItemClickListener { _, _, i, _ ->
                val selectedProduct: AirplaneDataItem =
                    listView.getItemAtPosition(i) as AirplaneDataItem
                idSelectedProduct = selectedProduct.id.toLong()
                Toast.makeText(
                    this@AirplaneDelete,
                    "Selected product: $selectedProduct",
                    Toast.LENGTH_LONG
                ).show();

                Log.d("Selected", selectedProduct.toString())
            }

        delete.setOnClickListener {
            val call: Call<AirplaneDataItem> =
                retrofitBuilder.deleteData(idSelectedProduct)
            call.enqueue(object : Callback<AirplaneDataItem?> {
                override fun onResponse(
                    call: Call<AirplaneDataItem?>,
                    response: Response<AirplaneDataItem?>
                ) {
                    Toast.makeText(
                        this@AirplaneDelete,
                        "Product deleted!",
                        Toast.LENGTH_LONG
                    ).show()
                }

                override fun onFailure(call: Call<AirplaneDataItem?>, t: Throwable) {
                    Log.d("Activity DELETE", "ON FAILURE$call")
                    t.printStackTrace()
                }
            })
        }
    }
}