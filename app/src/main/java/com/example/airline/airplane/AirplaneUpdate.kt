package com.example.airline.airplane

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AirplaneUpdate : AppCompatActivity() {
    private lateinit var selectedProduct: AirplaneDataItem

    /**
     * Calls the method that gets all the airplanes
     * Mounts the into a view
     * After selection, sends the data forward yto next activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airplane_update)

        val listView: ListView = findViewById(R.id.listViewUpdate)
        var list: List<AirplaneDataItem> = ArrayList()

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirplaneApiInterface::class.java)

        val data = retrofitBuilder.getData()

        data.enqueue(object : Callback<List<AirplaneDataItem>?> {
            override fun onResponse(
                call: Call<List<AirplaneDataItem>?>,
                response: Response<List<AirplaneDataItem>?>
            ) {
                val products: List<AirplaneDataItem>? = response.body()
                if (products != null) {
                    list = products
                }
                val arrayAdapter: ArrayAdapter<AirplaneDataItem?> = ArrayAdapter<AirplaneDataItem?>(
                    applicationContext, android.R.layout.simple_list_item_1, list
                )
                listView.adapter = arrayAdapter
            }

            override fun onFailure(call: Call<List<AirplaneDataItem>?>, t: Throwable) {
                Toast.makeText(this@AirplaneUpdate, "No airplanes in db", Toast.LENGTH_LONG).show();
            }
        })

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, i, _ ->
                val selectedProductFromView: AirplaneDataItem =
                    listView.getItemAtPosition(i) as AirplaneDataItem
                selectedProduct = selectedProductFromView
                Log.d("Selected", selectedProduct.toString())

                val intent = Intent(this, UpdateForm::class.java);

                intent.putExtra("id", selectedProduct.id)
                intent.putExtra("model", selectedProduct.model)
                intent.putExtra("capacity", selectedProduct.capacity)
                intent.putExtra("productionDate", selectedProduct.productionDate)
                intent.putExtra("uniqueId", selectedProduct.uniqueId)
                startActivity(intent)
            }
    }
}