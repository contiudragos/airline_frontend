package com.example.airline.airplane

import retrofit2.Call
import retrofit2.http.*

/**
 * This interface contains the main methods for our REST API
 */
interface AirplaneApiInterface {

    @GET("airplane")
    fun getData(): Call<List<AirplaneDataItem>>

    @POST("airplane")
    fun postData(
        @Body airplane: AirplaneDataItem
    ): Call<AirplaneDataItem>

    @DELETE("airplane/{id}")
    fun deleteData(@Path("id") id: Long): Call<AirplaneDataItem>

    @PUT("airplane")
    fun updateData(@Body airplane: AirplaneDataItem): Call<AirplaneDataItem>
}