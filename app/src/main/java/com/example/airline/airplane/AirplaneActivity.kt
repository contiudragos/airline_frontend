package com.example.airline.airplane

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.airline.R

class AirplaneActivity : AppCompatActivity() {

    /**
     * The main activities for airplanes are coordinated from here
     * The user can see/add/update/delete airplanes
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airplane)

        val getButton = findViewById<Button>(R.id.get_airplane);
        val postButton = findViewById<Button>(R.id.post_airplane);
        val updateButton = findViewById<Button>(R.id.update_airplane);
        val deleteButton = findViewById<Button>(R.id.delete_airplane);

        getButton.width = 500;
        postButton.width = 500;
        updateButton.width = 500;
        deleteButton.width = 500;

        getButton.setOnClickListener {
            val intent = Intent(this, AirplaneTable::class.java)
            startActivity(intent)
        }

        postButton.setOnClickListener {
            val intent = Intent(this, AirplaneAddForm::class.java)
            startActivity(intent)
        }

        deleteButton.setOnClickListener {
            val intent = Intent(this, AirplaneDelete::class.java)
            startActivity(intent)
        }

        updateButton.setOnClickListener {
            val intent = Intent(this, AirplaneUpdate::class.java)
            startActivity(intent)
        }
    }
}