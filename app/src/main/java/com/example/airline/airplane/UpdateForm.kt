package com.example.airline.airplane

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class UpdateForm : AppCompatActivity() {

    /**
     * Update the interface with data received from the precedent activity
     * Send the request to update and handle the response
     */
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_form)

        val submitButton = findViewById<Button>(R.id.btn_add_airplane)
        val model = findViewById<EditText>(R.id.airplane_model)
        val date = findViewById<DatePicker>(R.id.airplane_date)
        val capacity = findViewById<EditText>(R.id.airplane_capacity)
        val uniqueId = findViewById<EditText>(R.id.airplane_unique_id)

        val airplaneId = intent.extras?.get("id")
        val airplaneModel = intent.extras?.get("model")
        val airplaneCapacity = intent.extras?.get("capacity")
        val airplaneProductionDate = intent.extras?.get("productionDate")
        val airplaneUniqueId = intent.extras?.get("uniqueId")

        model.setText(airplaneModel.toString())
        var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        formatter =
            formatter.withLocale(Locale("ENGLISH")) // Locale specifies human language for translating, and cultural norms for lowercase/uppercase and abbreviations and such. Example: Locale.US or Locale.CANADA_FRENCH
        val dateFromIntent: LocalDate = LocalDate.parse(
            airplaneProductionDate as CharSequence?, formatter
        )
        date.init(dateFromIntent.year, dateFromIntent.monthValue, dateFromIntent.dayOfMonth, null)
        capacity.setText(airplaneCapacity.toString())
        uniqueId.setText(airplaneUniqueId.toString())

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirplaneApiInterface::class.java)

        submitButton.setOnClickListener {
            val today = LocalDate.now()
            val givenDate = LocalDate.of(date.year, date.month, date.dayOfMonth)

            if (today < givenDate) {
                Toast.makeText(this@UpdateForm, "The date is in the future", Toast.LENGTH_LONG)
                    .show();
            } else {

                val airplane = AirplaneDataItem(
                    airplaneId as Int,
                    model.text.toString(),
                    "" + date.year + "-" + date.month + "-" + date.dayOfMonth,
                    Integer.parseInt(capacity.text.toString()),
                    uniqueId.text.toString()
                )

                val data = retrofitBuilder.updateData(airplane)
                data.enqueue(object : Callback<AirplaneDataItem> {
                    override fun onResponse(
                        call: Call<AirplaneDataItem>,
                        response: Response<AirplaneDataItem>
                    ) {
                        val intent = Intent(this@UpdateForm, AirplaneActivity::class.java);
                        startActivity(intent)
                    }

                    override fun onFailure(call: Call<AirplaneDataItem?>, t: Throwable) {
                        Log.d("Activity DELETE", "ON FAILURE$call")
                        t.printStackTrace()
                    }
                })
            }
        }
    }
}