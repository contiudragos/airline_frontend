package com.example.airline.airplane

/**
 * A class identical with the one on the backend in order to have a great communication
 */
data class AirplaneDataItem(
    val id: Int,
    val model: String,
    val productionDate: String,
    val capacity: Int,
    val uniqueId: String
)