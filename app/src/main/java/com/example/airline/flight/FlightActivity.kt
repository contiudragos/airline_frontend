package com.example.airline.flight

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.airline.R

class FlightActivity : AppCompatActivity() {

    /**
     * The main activities for flights are coordinated from here
     * The user can see/add/update/delete flights
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flight)
    }
}