package com.example.airline

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.airline.airplane.AirplaneActivity
import com.example.airline.airport.AirportActivity
import com.example.airline.employee.EmployeeActivity
import com.example.airline.flight.FlightActivity

class MainActivity : AppCompatActivity() {

    /**
     * The main program is structured here.
     * The user can see/add/update/delete airplanes, airports, flights and employees
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val airplanesButton = findViewById<Button>(R.id.Airplanes);
        val airportsButton = findViewById<Button>(R.id.Airports);
        val employeesButton = findViewById<Button>(R.id.Employees);
        val flightsButton = findViewById<Button>(R.id.Flights);

        airplanesButton.width = 500;
        airportsButton.width = 500;
        employeesButton.width = 500;
        flightsButton.width = 500;

        airplanesButton.setOnClickListener {
            val intent = Intent(this, AirplaneActivity::class.java)
            startActivity(intent)
        }

        airportsButton.setOnClickListener {
            val intent = Intent(this, AirportActivity::class.java)
            startActivity(intent)
        }

        employeesButton.setOnClickListener {
            val intent = Intent(this, EmployeeActivity::class.java)
            startActivity(intent)
        }

        flightsButton.setOnClickListener {
            val intent = Intent(this, FlightActivity::class.java)
            startActivity(intent)
        }
    }
}
