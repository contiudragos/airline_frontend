package com.example.airline.airport

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UpdateAirportForm : AppCompatActivity() {
    private val booleanOptions: Array<String> = arrayOf("Yes", "No")

    /**
     * Update the interface with data received from the precedent activity
     * Send the request to update and handle the response
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_form2)

        val airportId = intent.extras?.get("id")
        val airportName = intent.extras?.get("name")
        val airportLocation = intent.extras?.get("location")
        val airportIsAvailable = intent.extras?.get("isAvailable")
        val airportHasParking = intent.extras?.get("hasParking")

        val inputName = findViewById<EditText>(R.id.airport_name)
        val inputLocation = findViewById<EditText>(R.id.airport_location)

        inputName.setText(airportName.toString())
        inputLocation.setText(airportLocation.toString())

        var hasParking = 0
        var isAvailable = 0
        val addButton = findViewById<Button>(R.id.btn_add_airport)

        val arrayAdapter = ArrayAdapter(
            this@UpdateAirportForm,
            android.R.layout.simple_spinner_dropdown_item,
            booleanOptions
        )
        val spinner = findViewById<Spinner>(R.id.isAvailable)
        spinner.adapter = arrayAdapter
        if (airportIsAvailable == 1)
            spinner.setSelection(0)
        else
            spinner.setSelection(1)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener,
            AdapterView.OnItemClickListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                isAvailable = if (booleanOptions[position] == "Yes")
                    1;
                else
                    0;
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                isAvailable = 0
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                TODO("Not yet implemented")
            }

        }


        val spinnerParking = findViewById<Spinner>(R.id.hasParking)
        spinnerParking.adapter = arrayAdapter
        if (airportHasParking == 1)
            spinnerParking.setSelection(0)
        else
            spinnerParking.setSelection(1)
        spinnerParking.onItemSelectedListener = object : AdapterView.OnItemSelectedListener,
            AdapterView.OnItemClickListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                hasParking = if (booleanOptions[position] == "Yes")
                    1;
                else
                    0;
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                hasParking = 0
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                TODO("Not yet implemented")
            }
        }

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirportApiInterface::class.java)

        addButton.setOnClickListener {
            val airport = AirportDataItem(
                id = airportId as Int,
                name = inputName.text.toString(),
                location = inputLocation.text.toString(),
                isAvailable = isAvailable,
                hasParking = hasParking
            )

            val response = retrofitBuilder.updateData(airport)

            response.enqueue(object : Callback<AirportDataItem> {
                override fun onResponse(
                    call: Call<AirportDataItem>,
                    response: Response<AirportDataItem>
                ) {
                    if (response.isSuccessful) {
                        Toast.makeText(
                            this@UpdateAirportForm,
                            "Successfully updated",
                            Toast.LENGTH_LONG
                        ).show();
                    } else {
                        Toast.makeText(
                            this@UpdateAirportForm,
                            "There is already an airport with this name in this location",
                            Toast.LENGTH_LONG
                        ).show();
                    }
                }

                override fun onFailure(call: Call<AirportDataItem>, t: Throwable) {
                    Toast.makeText(
                        this@UpdateAirportForm,
                        "Successfully updated",
                        Toast.LENGTH_LONG
                    ).show()
                }

            })
        }
    }
}