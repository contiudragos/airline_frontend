package com.example.airline.airport

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AirportDelete : AppCompatActivity() {

    /**
     * Creates a view that displays all the airplanes
     * Select an airplane an display a message
     * Call the delete method from the interface
     * Handle the response
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        var idSelectedProduct: Long = 0

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airport_delete)

        val delete = findViewById<View>(R.id.deleleDeleteProductBtn)
        val listView: ListView = findViewById(R.id.listViewDelete)
        var list: List<AirportDataItem> = ArrayList()

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirportApiInterface::class.java)

        val data = retrofitBuilder.getData()

        data.enqueue(object : Callback<List<AirportDataItem>?> {
            override fun onResponse(
                call: Call<List<AirportDataItem>?>,
                response: Response<List<AirportDataItem>?>
            ) {
                val products: List<AirportDataItem>? = response.body()
                if (products != null) {
                    list = products
                }
                val arrayAdapter: ArrayAdapter<AirportDataItem?> = ArrayAdapter<AirportDataItem?>(
                    applicationContext, android.R.layout.simple_list_item_1, list
                )
                listView.adapter = arrayAdapter
            }

            override fun onFailure(call: Call<List<AirportDataItem>?>, t: Throwable) {
                Toast.makeText(this@AirportDelete, "No airports in db", Toast.LENGTH_LONG).show();
            }
        })

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, i, _ ->
                val selectedProduct: AirportDataItem =
                    listView.getItemAtPosition(i) as AirportDataItem
                idSelectedProduct = selectedProduct.id.toLong()
                Toast.makeText(
                    this@AirportDelete,
                    "Selected product: $selectedProduct",
                    Toast.LENGTH_LONG
                ).show();

                Log.d("Selected", selectedProduct.toString())
            }

        delete.setOnClickListener {
            val call: Call<AirportDataItem> =
                retrofitBuilder.deleteData(idSelectedProduct)
            call.enqueue(object : Callback<AirportDataItem?> {
                override fun onResponse(
                    call: Call<AirportDataItem?>,
                    response: Response<AirportDataItem?>
                ) {
                    Toast.makeText(
                        this@AirportDelete,
                        "Product deleted!",
                        Toast.LENGTH_LONG
                    ).show()
                }

                override fun onFailure(call: Call<AirportDataItem?>, t: Throwable) {
                    Log.d("Activity DELETE", "ON FAILURE$call")
                    t.printStackTrace()
                }
            })
        }
    }
}