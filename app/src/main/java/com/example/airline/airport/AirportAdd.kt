package com.example.airline.airport

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class AirportAdd : AppCompatActivity() {
    private val booleanOptions: Array<String> = arrayOf("Yes", "No")

    /**
     * This function renders the form which allows the user to add data
     * Check the validity of the data
     * Send the request to the backend
     * Handle the response received
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airport_add)

        var hasParking = 0
        var isAvailable = 0
        val addButton = findViewById<Button>(R.id.btn_add_airport)

        val arrayAdapter = ArrayAdapter(
            this@AirportAdd,
            android.R.layout.simple_spinner_dropdown_item,
            booleanOptions
        )
        val spinner = findViewById<Spinner>(R.id.isAvailable)
        spinner.adapter = arrayAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener,
            AdapterView.OnItemClickListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                isAvailable = if (booleanOptions[position] == "Yes")
                    1;
                else
                    0;
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                isAvailable = 0
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                TODO("Not yet implemented")
            }

        }


        val spinnerParking = findViewById<Spinner>(R.id.hasParking)
        spinnerParking.adapter = arrayAdapter
        spinnerParking.onItemSelectedListener = object : AdapterView.OnItemSelectedListener,
            AdapterView.OnItemClickListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                hasParking = if (booleanOptions[position] == "Yes")
                    1;
                else
                    0;
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                hasParking = 0
            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                TODO("Not yet implemented")
            }
        }

        addButton.setOnClickListener {
            val inputName = findViewById<EditText>(R.id.airport_name)
            val inputLocation = findViewById<EditText>(R.id.airport_location)

            val airport = AirportDataItem(
                id = -1,
                name = inputName.text.toString(),
                location = inputLocation.text.toString(),
                isAvailable = isAvailable,
                hasParking = hasParking
            )

            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://10.0.2.2:8080/api/v1/")
                .build()
                .create(AirportApiInterface::class.java)

            val response = retrofitBuilder.postData(airport)

            response.enqueue(object : Callback<AirportDataItem> {
                override fun onResponse(
                    call: Call<AirportDataItem>,
                    response: Response<AirportDataItem>
                ) {
                    if (response.isSuccessful) {
                        Toast.makeText(this@AirportAdd, "Successfully added", Toast.LENGTH_LONG)
                            .show();
                    } else {
                        Toast.makeText(
                            this@AirportAdd,
                            "There is already an airport with this name in this location",
                            Toast.LENGTH_LONG
                        ).show();
                    }
                }

                override fun onFailure(call: Call<AirportDataItem>, t: Throwable) {
                    Toast.makeText(this@AirportAdd, "Successfully added", Toast.LENGTH_LONG).show()
                }

            })
        }
    }
}