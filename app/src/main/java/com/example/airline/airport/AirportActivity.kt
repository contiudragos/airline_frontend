package com.example.airline.airport

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.airline.R

class AirportActivity : AppCompatActivity() {

    /**
     * The main activities for airports are coordinated from here
     * The user can see/add/update/delete airports
    */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airport)

        val getButton = findViewById<Button>(R.id.get_airport);
        val postButton = findViewById<Button>(R.id.post_airport);
        val updateButton = findViewById<Button>(R.id.update_airport);
        val deleteButton = findViewById<Button>(R.id.delete_airport);

        getButton.width = 500;
        postButton.width = 500;
        updateButton.width = 500;
        deleteButton.width = 500;

        getButton.setOnClickListener {
            val intent = Intent(this, AirportTable::class.java)
            startActivity(intent)
        }

        postButton.setOnClickListener {
            val intent = Intent(this, AirportAdd::class.java)
            startActivity(intent)
        }

        updateButton.setOnClickListener {
            val intent = Intent(this, AirportUpdate::class.java)
            startActivity(intent)
        }

        deleteButton.setOnClickListener {
            val intent = Intent(this, AirportDelete::class.java)
            startActivity(intent)
        }
    }
}