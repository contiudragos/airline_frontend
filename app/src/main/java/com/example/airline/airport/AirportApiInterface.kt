package com.example.airline.airport

import retrofit2.Call
import retrofit2.http.*

/**
 * This interface contains the main methods for our REST API
 */
interface AirportApiInterface {
    @GET("airport")
    fun getData(): Call<List<AirportDataItem>>

    @POST("airport")
    fun postData(
        @Body airport: AirportDataItem
    ): Call<AirportDataItem>

    @DELETE("airport/{id}")
    fun deleteData(@Path("id") id: Long): Call<AirportDataItem>

    @PUT("airport")
    fun updateData(@Body airport: AirportDataItem): Call<AirportDataItem>
}