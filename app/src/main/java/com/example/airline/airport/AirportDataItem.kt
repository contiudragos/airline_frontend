package com.example.airline.airport

/**
 * A class identical with the one on the backend in order to have a great communication
 */
data class AirportDataItem(
    val hasParking: Int,
    val id: Int,
    val isAvailable: Int,
    val location: String,
    val name: String
)