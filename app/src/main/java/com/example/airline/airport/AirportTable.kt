package com.example.airline.airport

import android.annotation.SuppressLint
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AirportTable : AppCompatActivity() {

    /**
     * Call the method that gets all the airports
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airport_table)

        getAirports()
    }

    /**
     * Gets all the airports and mount them into a list
     */
    private fun getAirports() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirportApiInterface::class.java)

        val data = retrofitBuilder.getData()

        data.enqueue(object : Callback<List<AirportDataItem>?> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<List<AirportDataItem>?>,
                response: Response<List<AirportDataItem>?>
            ) {
                val responseBody = response.body()!!

                val table = findViewById<TableLayout>(R.id.table_airport)

                for (data in responseBody) {
                    val tableRow = TableRow(this@AirportTable)
                    tableRow.setPadding(0, 5, 0, 5)
                    val id = TextView(this@AirportTable)
                    id.text = data.id.toString()
                    id.setTextColor(Color.WHITE)
                    id.textSize = 12f

                    tableRow.addView(id)

                    val name = TextView(this@AirportTable)
                    name.text = data.name
                    name.textSize = 12f
                    name.setTextColor(Color.WHITE)
                    tableRow.addView(name)

                    val location = TextView(this@AirportTable)
                    location.text = data.location
                    location.setTextColor(Color.WHITE)
                    location.textSize = 12F
                    tableRow.addView(location)

                    val isAvailable = TextView(this@AirportTable)
                    if (data.isAvailable.toString() == "1")
                        isAvailable.text = "YES"
                    else
                        isAvailable.text = "NO"
                    isAvailable.setTextColor(Color.WHITE)
                    isAvailable.textSize = 12f
                    tableRow.addView(isAvailable)

                    val hasParking = TextView(this@AirportTable)
                    if (data.hasParking.toString() == "1")
                        hasParking.text = "YES"
                    else
                        hasParking.text = "NO"
                    hasParking.setTextColor(Color.WHITE)
                    hasParking.textSize = 12f
                    tableRow.addView(hasParking)

                    table.addView(tableRow)
                }

            }

            override fun onFailure(call: Call<List<AirportDataItem>?>, t: Throwable) {
                Log.d("MainActivity", "Error while fetching " + t.message)
            }
        })
    }
}