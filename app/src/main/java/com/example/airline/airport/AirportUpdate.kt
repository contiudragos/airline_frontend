package com.example.airline.airport

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.example.airline.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AirportUpdate : AppCompatActivity() {
    private lateinit var selectedProduct: AirportDataItem

    /**
     * Calls the method that gets all the airplanes
     * Mounts the into a view
     * After selection, sends the data forward yto next activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airport_update)

        val listView: ListView = findViewById(R.id.listViewUpdate)
        var list: List<AirportDataItem> = ArrayList()

        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://10.0.2.2:8080/api/v1/")
            .build()
            .create(AirportApiInterface::class.java)

        val data = retrofitBuilder.getData()

        data.enqueue(object : Callback<List<AirportDataItem>?> {
            override fun onResponse(
                call: Call<List<AirportDataItem>?>,
                response: Response<List<AirportDataItem>?>
            ) {
                val products: List<AirportDataItem>? = response.body()
                if (products != null) {
                    list = products
                }
                val arrayAdapter: ArrayAdapter<AirportDataItem?> = ArrayAdapter<AirportDataItem?>(
                    applicationContext, android.R.layout.simple_list_item_1, list
                )
                listView.adapter = arrayAdapter
            }

            override fun onFailure(call: Call<List<AirportDataItem>?>, t: Throwable) {
                Toast.makeText(this@AirportUpdate, "No airports in db", Toast.LENGTH_LONG).show();
            }
        })

        listView.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, i, _ ->
                val selectedProductFromView: AirportDataItem =
                    listView.getItemAtPosition(i) as AirportDataItem
                selectedProduct = selectedProductFromView
                Log.d("Selected", selectedProduct.toString())

                val intent = Intent(this, UpdateAirportForm::class.java);

                intent.putExtra("id", selectedProduct.id)
                intent.putExtra("name", selectedProduct.name)
                intent.putExtra("location", selectedProduct.location)
                intent.putExtra("isAvailable", selectedProduct.isAvailable)
                intent.putExtra("hasParking", selectedProduct.hasParking)
                startActivity(intent)
            }
    }
}
