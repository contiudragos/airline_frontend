package com.example.airline.employee

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.airline.R

class EmployeeActivity : AppCompatActivity() {

    /**
     * The main activities for employees are coordinated from here
     * The user can see/add/update/delete employees
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee)

        val getButton = findViewById<Button>(R.id.get_employee);
        val postButton = findViewById<Button>(R.id.post_employee);
        val updateButton = findViewById<Button>(R.id.update_employee);
        val deleteButton = findViewById<Button>(R.id.delete_employee);

        getButton.width = 500;
        postButton.width = 500;
        updateButton.width = 500;
        deleteButton.width = 500;
    }
}